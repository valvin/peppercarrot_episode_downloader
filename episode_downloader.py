#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path

import argparse
import re
import requests
import zipfile

config = {
    'url':'https://www.peppercarrot.com/0_sources/',
    'dir':'.download'
}

def define_arguments():
    parser = argparse.ArgumentParser(description='Download Pepper & Carrot webcomics')
    parser.add_argument('-e', '--episode', type=int, nargs='+', help='List of episodes to download')
    parser.add_argument('-l', '--language', help='Language of the comic', default='fr')
    parser.add_argument('-c', '--catalog', action='store_true', help='List of existing episodes')
    return parser.parse_args()

class Episode():
    def __init__(self, name=None, url=None, translated_languages=None, number=None, total_page=None):
        self.name = name
        self.number = number
        self.total_page = total_page
        self.translated_languages = translated_languages
        self.url = url
        if isinstance(name, dict):
            self.init_from_dict(name)
    def _clean_files(self, download_dir):
        for file in self._files:
            file_path = Path('.') / download_dir / file['name']
            file_path.unlink()
    def _download_files(self, download_dir):
        dir_path = Path('.') / download_dir
        if not dir_path.is_dir():
            dir_path.mkdir(parents=True)
        for file in self._files:
            ouput_file = dir_path / file['name']
            with ouput_file.open(mode='wb') as f:
                r = requests.get(file['url'], stream=True)
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
    def _get_files_list(self, language):
        file_regex = "<a href=\"" + language + "(.+?)\">"
        pattern = re.compile(file_regex)
        r = requests.get(self.url)
        html = r.content.decode()
        files = re.findall(pattern, html)
        self._files = []
        for file in files:
            new_file = {}
            new_file['name'] = language + file
            new_file['url'] = self.url + new_file['name']
            self._files.append(new_file)
    def _merge_files(self, download_dir, language):
        target_filename = 'peppercarrot_ep{:02}_{}_{}.cbz'.format(self.number, self.name[5:], language)
        with zipfile.ZipFile(target_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
            for file in self._files:
                file_path = Path('.') / download_dir / file['name']
                zipf.write(str(file_path))
    def download(self, language, download_dir):
        if self.translated_languages == None:
            raise Exception('Episode not configurated')
        if not language in self.translated_languages:
            raise Exception('The requested translation is not available')
        self._get_files_list(language)
        self._download_files(download_dir)
        self._merge_files(download_dir, language)
        self._clean_files(download_dir)
    def init_from_dict(self, config):
        if 'name' in config:
            self.name = config['name']
        if 'number' in config:
            self.number = config['number']
        if 'total_page' in config:
            self.total_page = config['total_page']
        if 'translated_languages' in config:
            self.translated_languages = config['translated_languages']
        if 'url' in config:
            self.url = config['url']

class EpisodeList():
    def __init__(self, url=None):
        self.url = url
        self.episodes_list = []
    def __repr__(self):
        result = ''
        for episode in self.episodes_list:
            result += ' {: >2} : {}\n'.format(episode['number'], episode['name'][5:].replace('-', ' '))
        return result
    def generate(self):
        r = requests.get(self.url + "episodes.json")
        if r.status_code != 200:
            raise Exception('The requested resource was not found')
        l = r.json()
        for e in l:
            e['number'] = int(e['name'][2:4])
            e['url'] = '{}{}/hi-res/'.format(self.url,e['name'])
            self.episodes_list.append(e)
    def get(self, number):
        e = next((x for x in self.episodes_list if x['number'] == number), None)
        if e == None:
            raise Exception("Asked episode {} does'nt exist".format(number))
        return e

if __name__ == '__main__':
    args = define_arguments()
    el = EpisodeList(config['url'])
    el.generate()
    if args.catalog:
        print(el)
    elif args.episode:
        for en in args.episode:
            print('Getting episode {} in {}...'.format(en,args.language))
            try:
                e = Episode(el.get(en))
                e.download(args.language,config['dir'])
            except Exception as ex:
                print(ex)
        print('')
